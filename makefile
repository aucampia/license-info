
test:
	./generate.py -l 0BSD -l UNLICENSED -f JINJA2 --force
	python3 -m pytest --log-cli-level=DEBUG run-tests.py -vv

test-only:
	python3 -m pytest --log-cli-level=DEBUG run-tests.py -vv

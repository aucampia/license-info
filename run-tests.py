#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:

# python3 -m pytest --log-cli-level=DEBUG run-tests.py
# pydoc3 file_uri_to_path

import pytest
import yaml
import jinja2
import jinja2.sandbox
import os.path
import logging
import dataclasses

logger = logging.getLogger(__name__)

@dataclasses.dataclass
class TestParameters:
    license_code: str
    width: 'typing.Any'
    suffix: str
    test_name: str
    input_data: str = None
    output_content: str = None

    @property
    def ext(self):
        return self.suffix

    @classmethod
    def from_args(cls, license_code, width, ext, test_name, *, input_data=None, output_content=None):
        return cls(license_code, width, ext, test_name, input_data, output_content)

def lictext(license_code, width):
    text_file = os.path.join("licenses", license_code,
        "template-width_{}.{}".format(width, "txt"))
    with open(text_file, "rb") as file_handle:
        return file_handle.read().decode("utf-8")

test_template_inputs = [
    TestParameters.from_args("UNLICENSED","CANON", "cc.j2", "BLANK", input_data={
            "cookiecutter": {}
        },
        output_content="Copyright (c)  \n"),
    TestParameters.from_args("UNLICENSED","CANON", "j2", "BLANK",
        input_data={"license_copyright_header": "Copyright (c) Iwan Aucamp"},
        output_content="Copyright (c) Iwan Aucamp\n"),
    TestParameters.from_args("UNLICENSED","CANON", "j2", "BLANK",
        input_data={
            "license_copyright_holder": "Iwan Aucamp",
            "license_copyright_years": "2019",
        },
        output_content="Copyright (c) 2019 Iwan Aucamp\n"),
    TestParameters.from_args("0BSD","CANON", "j2", "BLANK",
        input_data={
            "license_copyright_holder": "Iwan Aucamp",
            "license_copyright_years": "2019",
        },
        output_content="".join([
            f"Copyright (c) 2019 Iwan Aucamp\n\n{lictext('0BSD', 'CANON')}",
            ])),
    TestParameters.from_args("0BSD","CANON", "j2", "BLANK",
        input_data={
            "license_preamble": "PREAMBLE",
            "license_copyright_holder": "Iwan Aucamp",
            "license_copyright_years": "2019",
            "license_appendix": "\nAPPENDIX",
        },
        output_content="".join([
            f"PREAMBLE",
            f"Copyright (c) 2019 Iwan Aucamp\n\n{lictext('0BSD', 'CANON')}",
            f"APPENDIX\n",
            ])),
    TestParameters.from_args("0BSD","CANON", "j2", "BLANK",
        input_data={
            "license_preamble": "\n\nPREAMBLE\n\n",
            "license_copyright_holder": "Iwan Aucamp",
            "license_copyright_years": "2019",
            "license_appendix": "\n\nAPPENDIX\n\n",
        },
        output_content="".join([
            f"\n\nPREAMBLE\n\n",
            f"Copyright (c) 2019 Iwan Aucamp\n\n{lictext('0BSD', 'CANON')}",
            f"\nAPPENDIX\n\n",
            f"\n"])),
]

def idfn(parameters):
    return parameters.test_name

@pytest.mark.parametrize("parameters", test_template_inputs, ids=idfn)
def test_template(parameters):
    return do_test_template(parameters.license_code, parameters.width, parameters.ext,
        parameters.test_name, parameters.input_data, parameters.output_content)

def do_test_template(license_code, width, ext, test_name, input_data=None, output_content=None):
    template_file = os.path.join("licenses", license_code,
        "template-width_{}.{}".format(width, ext))
    logger.info("Test %s with %s", test_name, template_file)

    with open(template_file, "rb") as file_handle:
        template_content = file_handle.read().decode("utf-8")

    if input_data is None:
        input_file = os.path.join("tests",
            "test-{}-width_{}-{}-{}-in.yaml".format(license_code, width, ext, test_name))
        with open(input_file, "rb") as file_handle:
            input_content = file_handle.read().decode("utf-8")
            input_data = yaml.safe_load(input_content)

    if output_content is None:
        output_file = os.path.join("tests",
            "test-{}-width_{}-{}-{}-out.txt".format(license_code, width, ext, test_name))
        with open(output_file, "rb") as file_handle:
            output_content = file_handle.read().decode("utf-8")

    env = jinja2.sandbox.SandboxedEnvironment(keep_trailing_newline=True)
    template = env.from_string(template_content);

    rendered = template.render(**input_data)

    logger.debug("\nSTART>>>%s<<<END\n", output_content);

    assert output_content == rendered

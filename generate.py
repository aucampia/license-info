#!/usr/bin/env python3

# vim: set filetype=python tw=100 cc=+1:
"""
Command line interface module
"""

# pylint: disable=too-many-locals
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-few-public-methods
import logging
import argparse
import sys
import types
import enum
import types
import json
import os
import pathlib
import urllib.request
import lxml
import lxml.etree
import html2text
import textwrap
import re


script_path = pathlib.Path(__file__)
script_dirname = script_path.parent
script_dirname_abs = script_dirname.resolve()
script_basename = script_path.name


logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%dT%H:%M:%S", stream=sys.stderr,
    format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))

logger = logging.getLogger(__name__)

license_map = {}

class FormatMetadata:
    def __init__(self, extention):
        self.extention = extention

class TemplateFormat(enum.Enum):
    TEXT = FormatMetadata(extention="txt")
    ENVSUSBT_TEXT = enum.auto()
    ENVSUBST_VARS = enum.auto()
    ENVSUBST_LINE = enum.auto()
    JINJA2 = FormatMetadata(extention="j2")
    JINJA2_COOKIECUTTER = FormatMetadata(extention="cc.j2")
    MUSTACHE = enum.auto()

def static_init(cls):
    logger.info("cls = %s", cls)
    if getattr(cls, "__static_init__", None):
        cls.__static_init__()
    return cls

def unwrap_text(text):
    return "\n\n".join(unwrap_text_to_paragraphs(text))

def unwrap_text_to_paragraphs(text):
    paragraphs = [""]
    lines = text.splitlines()
    line_previous = None
    for line in lines:
        if line_previous is not None \
            and ( not line_previous or line_previous.isspace() ) \
            and paragraphs[-1]:
            paragraphs.append("")
        line_previous = line
        if not line:
            continue
        if paragraphs[-1]:
            paragraphs[-1] += " "
        paragraphs[-1] += line.rstrip()
    return paragraphs

def wrap_paragraphs(paragraphs, width):
    if width is None:
        return paragraphs
    return ["\n".join(textwrap.wrap(paragraph, width)) for paragraph in paragraphs]

def wrap_text(text, width):
    paragraphs = unwrap_text_to_paragraphs(text)
    return "\n\n".join(wrap_paragraphs(paragraphs, width))

license_list = []
license_dict = {}

@static_init
class License(types.SimpleNamespace):

    @classmethod
    def __static_init__(cls):
        logger.info("cls = %s", cls)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._name = None
        self.url = None
        self.version = None
        self.canonical_width = None

    @property
    def code(self):
        return self.spdx_code

    @property
    def name(self):
        if self._name is not None:
            return self._name
        return self.spdx_name

    """
    def __init__(self, *, spdx_code, spdx_name, url, name, get_template, **kwargs):
        logger.info("spdx_code = %s", spdx_code)
        super().__init__(kwargs)
        if spdx_code in license_map:
            raise RuntimeError("sdpx_code {} already defined".format(spdx_code))
        license_map[spdx_code] = self
        self.spdx_code = spdx_code
        self.spdx_name = spdx_name
        self.name = name
        self.url = url
        self._get_template = get_template
    """

    def get_template(self, format, text_width=None):
        """
        if getattr(self, "_get_template", None):
            return self._get_template(format, text_width)
        """
        if format is TemplateFormat.JINJA2:
            license_body = self.get_template(TemplateFormat.TEXT, text_width)
            if license_body is None:
                return None
            logger.debug("license_body = %s", license_body);
            lines = []
            if len(license_body) > 0:
                lines.extend([
                    "{%- set license_body %}",
                    "",
                    license_body,
                    "{%- endset -%}",
                ])
            else:
                lines.extend(["{%- set license_body = \"\" -%}"])
            lines.extend([
                "{%- if not license_copyright_header -%}",
                "{%- set license_copyright_header -%}",
                ("Copyright (c) {{ license_copyright_years }} "
                    "{{ license_copyright_holder }}{{ license_copyright_suffix | default('') }}"),
                "{%- endset -%}",
                "{%- endif -%}",
                ("{{ license_preamble | default('') }}"
                    "{{ license_copyright_header -}}"),
                ("{{ license_body }}"
                    "{{ license_appendix | default('') }}"),
            ])
            template = "\n".join(lines)
            logger.debug("template = %s", template);
            return template;
        if format is TemplateFormat.JINJA2_COOKIECUTTER:
            license_body = self.get_template(TemplateFormat.JINJA2, text_width)
            if license_body is None:
                return None
            logger.debug("license_body = %s", license_body);
            lines = []
            lines.extend([
                ("{%- set license_copyright_header = "
                    "cookiecutter.license_copyright_header -%}"),
                ("{%- set license_copyright_holder = "
                    "cookiecutter.license_copyright_holder -%}"),
                ("{%- set license_copyright_years = "
                    "cookiecutter.license_copyright_years -%}"),
                ("{%- set license_copyright_suffix = "
                    "cookiecutter.license_copyright_suffix -%}"),
                ("{%- set license_preamble = "
                    "cookiecutter.license_preamble -%}"),
                ("{%- set license_appendix = "
                    "cookiecutter.license_appendix -%}"),
                license_body
            ])
            template = "\n".join(lines)
            logger.debug("template = %s", template);
            return template;
        logger.warning("No template for code = %s, format = %s, text_width = %s",
            self.code, format, text_width);
        return None

    def get_metadata(self, extra={}):
        result = {
            "spdx_code": self.spdx_code,
            "spdx_name": self.spdx_name,
            "name": self.name,
            "url": self.url,
            **extra,
        }
        return result

    @property
    def metadata(self):
        return self.get_metadata()

    def write_metadata(self, base_path, force=False):
        license_dir = base_path.joinpath(self.code)
        license_dir.mkdir(parents=True, exist_ok=True)
        license_file = license_dir.joinpath("{}.json".format(self.code))
        if not force and license_file.exists():
            logging.info("output already exists, not writing: %s", license_file)
            return
        logging.info("writing %s", license_file)
        with open(license_file, "w+") as file_handle:
            file_handle.write(json.dumps(self.metadata, sort_keys=True, indent=2))
            file_handle.write("\n")

    def write_template(self, base_path, format, text_width=None, force=False):
        license_dir = base_path.joinpath(self.code)
        license_dir.mkdir(parents=True, exist_ok=True)
        license_filename="template"
        if text_width == "CANON":
            license_filename += "-width_CANON".format(text_width)
        elif text_width is not None:
            license_filename += "-width_{:04d}".format(text_width)
        else:
            license_filename += "-width_NONE"
        license_filename += "."
        license_filename += format.value.extention
        license_file = license_dir.joinpath(license_filename)
        if not force and license_file.exists():
            logging.info("output already exists, not writing: %s", license_file)
            return
        logging.info("writing %s", license_file)
        content = self.get_template(format, text_width)
        if content is None:
            logger.warning("Not writing output for code = %s, format = %s, text_width = %s",
                self.code, format, text_width);
            return
        with open(license_file, "w+") as file_handle:
            file_handle.write(content)
            file_handle.write("\n")

    def __init_subclass__(cls, **kwargs):
        #logger.info("cls = %s", cls)
        #logger.info("kwargs = %s", kwargs)
        #logger.info("cls.spdx_code = %s", cls.spdx_code)
        license_list.append(cls)
        License.add_license(cls)

    @classmethod
    def add_license(cls, LicenseClass):
        license = LicenseClass()
        logger.info("license.spdx_code = %s", license.spdx_code)
        if license.spdx_code in license_dict:
            raise RuntimeError("Already have license with SDPX code {}".format(license.spdx_code))
        license_dict[license.spdx_code] = license

    @classmethod
    def load(cls):
        for license in license_list:
            add_license(license)


class LicenseUNLICENSED(License):

    def __init__(self):
        super().__init__()
        self.spdx_code = "UNLICENSED"
        self.spdx_name = "Unlicensed"
        self._name = "Unlicensed"

    def get_template(self, format, text_width=None):
        if format is TemplateFormat.TEXT:
            return ""
        return super().get_template(format, text_width)

class License0BSD(License):

    def __init__(self):
        super().__init__()
        self.spdx_code = "0BSD"
        self.spdx_name = "BSD Zero Clause License"
        self.url = "http://landley.net/toybox/license.html"
        self._name = "Zero Clause BSD license"
        self.canonical_width = 72

    def get_template(self, format, text_width=None):
        if format is TemplateFormat.TEXT:
            with urllib.request.urlopen("https://landley.net/toybox/license.html") as url:
                body = url.read()
            logger.debug("body = %s", body)
            body_tree = lxml.etree.HTML(body);
            license_quote = body_tree.xpath("/html/body/table/tr/td[2]/blockquote")[0];
            logger.debug("license_quote = %s", license_quote)
            license_quote.remove(list(license_quote)[0])
            license_quote.tag="div"
            logger.debug("license_quote = %s", license_quote)
            license_quote_xmlstring = lxml.etree.tostring(license_quote).decode("utf-8");
            logger.debug("license_quote = %s", license_quote_xmlstring)
            h2t = html2text.HTML2Text(bodywidth=self.canonical_width if text_width == "CANON" else text_width)
            license_text = h2t.handle(license_quote_xmlstring).strip()
            logger.debug("license_text = %s", license_text)
            return license_text
        return super().get_template(format, text_width)


class LicenseMIT0(License):

    def __init__(self):
        super().__init__()
        self.spdx_code = "MIT-0"
        self.spdx_name = "MIT No Attribution"
        self.url = "https://github.com/aws/mit-0"

    def get_template(self, format, text_width=None):
        if format is TemplateFormat.TEXT:
            with urllib.request.urlopen("https://raw.githubusercontent.com/aws/mit-0/master/MIT-0") as url:
                body = url.read().decode("utf-8")

            body_lines = body.splitlines()
            body_lines = body_lines[4:]

            canon_body = "\n".join(body_lines)
            if text_width == "CANON":
                return canon_body

            return wrap_text(canon_body, text_width)

        return super().get_template(format, text_width)

class LicenseMIT(License):

    def __init__(self):
        super().__init__()
        self.spdx_code = "MIT"
        self.spdx_name = "MIT License"

    def get_template(self, format, text_width=None):
        if format is TemplateFormat.TEXT:
            with urllib.request.urlopen("https://raw.githubusercontent.com/libexpat/libexpat/master/expat/COPYING") as url:
                body = url.read().decode("utf-8")

            body_lines = body.splitlines()
            body_lines = body_lines[3:]

            canon_body = "\n".join(body_lines)
            if text_width == "CANON":
                return canon_body

            return wrap_text(canon_body, text_width)

        return super().get_template(format, text_width)

class LicenseISC(License):

    def __init__(self):
        super().__init__()
        self.spdx_code = "ISC"
        self.spdx_name = "ISC License"
        self.url = "https://www.isc.org/licenses/"
        self.canonical_width = 75
        # https://raw.githubusercontent.com/isc-projects/forge/master/LICENSE.txt

    def get_template(self, format, text_width=None):
        if format is TemplateFormat.TEXT:
            with urllib.request.urlopen("https://opensource.org/licenses/ISC") as url:
                body = url.read()
            logger.debug("body = %s", body)
            body_tree = lxml.etree.HTML(body);
            license_quote = body_tree.xpath(("//*[@id='content-wrapper']//article"
                "//div[@class='field-item even']"))[0];
            logger.debug("license_quote = %s", license_quote)
            license_quote.remove(list(license_quote)[0])
            license_quote.tag="div"
            logger.debug("license_quote = %s", license_quote)
            license_quote_xmlstring = lxml.etree.tostring(license_quote).decode("utf-8");
            logger.debug("license_quote = %s", license_quote_xmlstring)
            h2t = html2text.HTML2Text(bodywidth=self.canonical_width if text_width == "CANON" else text_width)
            license_text = h2t.handle(license_quote_xmlstring).strip()
            logger.debug("license_text = %s", license_text)
            return license_text
        return super().get_template(format, text_width)

class LicenseCC0v1x0(License):

    def __init__(self):
        super().__init__()
        self.spdx_code = "CC0-1.0"
        self.spdx_name = "Creative Commons Zero v1.0 Universal"
        self.url = "https://creativecommons.org/publicdomain/zero/1.0/legalcode"
        self.canonical_width = 75

    def get_template(self, format, text_width=None):
        if format is TemplateFormat.TEXT:
            if text_width == "CANON":
                req = urllib.request.Request("https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt")
                req.add_header("User-Agent", "curl/7.65.3")
                with urllib.request.urlopen(req) as url:
                    return url.read().decode("utf-8")
            else:
                with urllib.request.urlopen("https://spdx.org/licenses/CC0-1.0.txt") as url:
                    unwrapped_body = url.read().decode("utf-8")
                unwrapped_body = re.sub("<<beginOptional[^>]*>>\s+", "", unwrapped_body)
                unwrapped_body = re.sub("\s+<<endOptional[^>]*>>\s+", "", unwrapped_body)
            return wrap_text(unwrapped_body, text_width)

        return super().get_template(format, text_width)

class Main:
    """
    Class for CLI Main
    """

    def __init__(self):
        self.apn_root = types.SimpleNamespace(parser=argparse.ArgumentParser(add_help=True))
        self.parse_result = None

    def exec(self):
        """
        Exec function for CLI Main
        """
        apn_current = apn_root = self.apn_root

        apn_current.parser.add_argument("-v", "--verbose",
            action="count", dest="verbosity",
            help="increase verbosity level")

        apn_current.parser.add_argument("-l", "--license",
            action="append", dest="licenses", default=[],
            help="select license")

        apn_current.parser.add_argument("-w", "--width",
            action="append", dest="widths",
            help="select widths")

        apn_current.parser.add_argument("-f", "--formats",
            action="append", dest="formats",
            help="select formats")

        apn_current.parser.add_argument("--force",
            action="store_true", dest="force", default=False,
            help="select widths")

        parse_result = self.parse_result = apn_root.parser.parse_args(args=sys.argv[1:])
        verbosity = parse_result.verbosity
        if verbosity is not None:
            root_logger = logging.getLogger("")
            root_logger.propagate = True
            new_level = (root_logger.getEffectiveLevel() -
                (min(1, verbosity)) * 10 - min(max(0, verbosity - 1), 9) * 1)
            root_logger.setLevel(new_level)

        logger.debug("sys.argv = %s, parse_result = %s, logging.level = %s, logger.level = %s",
            sys.argv, parse_result, logging.getLogger("").getEffectiveLevel(),
            logger.getEffectiveLevel())

        codes = parse_result.licenses if len(parse_result.licenses) > 0 else license_dict.keys()
        widths = [None, "CANON"]
        if parse_result.widths:
            widths = []
            for width in parse_result.widths:
                if width == "NONE":
                    widths.append(None)
                if width == "CANON":
                    widths.append("CANON")
                else:
                    widths.append(int(width))

        formats = [ TemplateFormat.TEXT, TemplateFormat.JINJA2, TemplateFormat.JINJA2_COOKIECUTTER ]
        if parse_result.formats:
            formats = []
            for format in parse_result.formats:
                formats.append(TemplateFormat[format])

        logger.info("codes = %s", codes);
        logger.info("widths = %s", widths);

        force = parse_result.force

        licenses_dir = script_dirname.joinpath("licenses")
        for code in codes:
            license = license_dict[code]
            license.write_metadata(licenses_dir, force)
            for text_width in widths:
                #if text_width == "CANON" and license.canonical_width is not None:
                #    text_width = license.canonical_width
                for format in formats:
                    logger.info("Processing %s,%s,%s", license.code, format, text_width);
                    license.write_template(licenses_dir, format, text_width, force)
            """
            license_dir = script_dirname.joinpath("licenses", code)
            license_dir.mkdir(parents=True, exist_ok=True)
            license_file = license_dir.joinpath("{}.json".format(code))
            logging.info("writing %s", license_file)
            with open(license_file, "w+") as file_handle:
                file_handle.write(json.dumps(license.metadata, sort_keys=True, indent=2))
                file_handle.write("\n")
            """

def exec_main():

    main = Main()
    main.exec()

if __name__ == "__main__":
    exec_main()


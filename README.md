# ...

## Template Engines

envsubst
m4
mustache
jinja2

```
reset

./generate.py -l 0BSD -l UNLICENSED -f JINJA2 --force

jinja2 --format=yaml licenses/0BSD/template-width_CANON.j2 data/input-vars.yml
jinja2 --format=yaml licenses/0BSD/template-width_NONE.j2 data/input-vars.yml
jinja2 --format=yaml licenses/0BSD/template-width_CANON.j2 data/input-header.yml
jinja2 --format=yaml licenses/0BSD/template-width_NONE.j2 data/input-header.yml

jinja2 --format=yaml licenses/UNLICENSED/template-width_CANON.j2 data/input-vars.yml
jinja2 --format=yaml licenses/UNLICENSED/template-width_NONE.j2 data/input-vars.yml
jinja2 --format=yaml licenses/UNLICENSED/template-width_CANON.j2 data/input-header.yml
jinja2 --format=yaml licenses/UNLICENSED/template-width_NONE.j2 data/input-header.yml
```
